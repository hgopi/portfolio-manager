import { Injectable } from "@angular/core";
import { Action, State, StateContext } from "@ngxs/store";
import { filter, finalize, tap } from "rxjs/operators";
import { HttpService } from "src/app/core/http.service";
import { SqliteService } from "src/app/core/sqlite.service";
import { AddStock, DeleteStock, EditStock, GetCategories, GetFundDetails, GetOneStockDetails, GetStockDetails, GetStocks, HideLoader, SearchMutualFunds, ShowLoader, StockAddedSuccessfully, StockDeleteSuccessfully } from "../actions/app.actions";

export interface AppStateModel {
  categories: any[];
  stockDetailsResult: any;
  stocks: any[];
  editStockData: any;
  searchMfResults: any[];
  fundDetails: any;
}

const initialState: AppStateModel = {
  categories: [],
  stockDetailsResult: null,
  stocks: [],
  editStockData: null,
  searchMfResults: [],
  fundDetails: null,
}

@State<AppStateModel>({
  name: 'app',
  defaults: initialState
})
@Injectable()
export class AppState {

  constructor(
    private sqlService: SqliteService,
    private httpService: HttpService
  ) {}

  @Action(GetCategories)
  getCategories({patchState}: StateContext<AppStateModel>) {
    return this.sqlService.getCategories().pipe(
      tap((response) => {
        patchState({categories: response});
      })
    );
  }

  @Action(AddStock)
  addStock({dispatch}: StateContext<AppStateModel>, { payload }: AddStock) {
    return this.sqlService.addStock(payload).pipe(
      tap((response) => {
        dispatch(new StockAddedSuccessfully());
      })
    );
  }

  @Action(EditStock)
  editStock({dispatch}: StateContext<AppStateModel>, { payload, id }: EditStock) {
    return this.sqlService.editdStock(payload, id).pipe(
      tap((response) => {
        dispatch(new StockAddedSuccessfully());
      })
    );
  }

  @Action(DeleteStock)
  deleteStock({dispatch}: StateContext<AppStateModel>, { id }: DeleteStock) {
    return this.sqlService.deleteStock(id).pipe(
      tap((response) => {
        dispatch(new StockDeleteSuccessfully());
      })
    );
  }

  @Action(GetStocks)
  getStocks({patchState}: StateContext<AppStateModel>) {
    return this.sqlService.getStocks().pipe(
      tap((response) => {
        console.log(response);
        patchState({
          stocks: response
        });
      })
    );
  }

  @Action(GetStockDetails)
  getStockDetails({patchState, dispatch}: StateContext<AppStateModel>, { symbol }: GetStockDetails) {
    dispatch(new ShowLoader());
    return this.httpService.getStockDetails(symbol).pipe(
      finalize(() => dispatch(new HideLoader())),
      tap((response) => {
        patchState({stockDetailsResult: response});
      })
    );
  }

  @Action(GetOneStockDetails)
  getOneStockDetails({patchState, dispatch}: StateContext<AppStateModel>, { id }: GetOneStockDetails) {
    dispatch(new ShowLoader());
    return this.sqlService.getOneStockDetails(id).pipe(
      finalize(() => dispatch(new HideLoader())),
      filter((response) => response?.length > 0),
      tap((response) => {
        patchState({editStockData: response[0]});
      })
    );
  }

  @Action(SearchMutualFunds)
  searchMutualFunds({patchState, dispatch}: StateContext<AppStateModel>, { query }: SearchMutualFunds) {
    dispatch(new ShowLoader());
    return this.httpService.searchFunds(query).pipe(
      finalize(() => dispatch(new HideLoader())),
      filter((response) => response?.length > 0),
      tap((response) => {
        patchState({searchMfResults: response});
      })
    );
  }

  @Action(GetFundDetails)
  getFundDetails({patchState, dispatch}: StateContext<AppStateModel>, { schemeCode }: GetFundDetails) {
    dispatch(new ShowLoader());
    return this.httpService.getFundDetails(schemeCode).pipe(
      finalize(() => dispatch(new HideLoader())),
      filter((response) => !!response),
      tap((response) => {
        patchState({fundDetails: response});
      })
    );
  }
  
}