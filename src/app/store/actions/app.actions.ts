export class GetCategories {
  static readonly type = '[APP] Get Categories';
}

export class ShowLoader {
  static readonly type = '[APP] Show Loader';
}

export class HideLoader {
  static readonly type = '[APP] Hide Loader';
}

export class GetStocks {
  static readonly type = '[APP] Get Stocks';
}

export class GetStockDetails {
  static readonly type = '[APP] Get Stock Details';
  constructor(public symbol: any) {}
}

export class AddStock {
  static readonly type = '[APP] Add Stock';
  constructor(public payload: any) {}
}

export class EditStock {
  static readonly type = '[APP] Edit Stock';
  constructor(public payload: any, public id: number) {}
}

export class DeleteStock {
  static readonly type = '[APP] Delete Stock';
  constructor(public id: number) {}
}

export class StockAddedSuccessfully {
  static readonly type = '[APP] Stock Added Successfully';
}

export class StockDeleteSuccessfully {
  static readonly type = '[APP] Stock Deleted Successfully';
}

export class GetOneStockDetails {
  static readonly type = '[APP] Get One Stock Details';
  constructor(public id: number) {}
}

export class SearchMutualFunds {
  static readonly type = '[APP] Search Mutual Funds';
  constructor(public query: string) {}
}

export class GetFundDetails {
  static readonly type = '[APP] Get Fund Details';
  constructor(public schemeCode: string | number) {}
}