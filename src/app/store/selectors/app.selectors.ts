import { Selector } from "@ngxs/store";
import { AppState, AppStateModel } from "../state/app.state";

export class AppSelectors {
  @Selector([AppState])
  static categories(state: AppStateModel) {
    return state.categories;
  }

  @Selector([AppState])
  static hasInvestments(state: AppStateModel) {
    return state.categories.some((category) => category.invested > 0);
  }

  @Selector([AppState])
  static getStockData(state: AppStateModel) {
    return state.categories.filter((category) => category.name === 'Stock')[0];
  }

  @Selector([AppState])
  static getStockDetailsResult(state: AppStateModel) {
    return state.stockDetailsResult;
  }

  @Selector([AppState])
  static stocks(state: AppStateModel) {
    return state.stocks;
  }

  @Selector([AppState])
  static editStockdata(state: AppStateModel) {
    return state.editStockData;
  }

  @Selector([AppState])
  static searchMfResult(state: AppStateModel) {
    return state.searchMfResults;
  }

  @Selector([AppState])
  static fundDetails(state: AppStateModel) {
    return state.fundDetails;
  }

}