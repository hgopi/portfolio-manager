import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { debounceTime, filter, map, startWith, switchMap } from 'rxjs/operators';
import { GetFundDetails, SearchMutualFunds } from 'src/app/store/actions/app.actions';
import { AppSelectors } from 'src/app/store/selectors/app.selectors';
import { parse, format } from 'date-fns'

@Component({
  selector: 'app-add-fund',
  templateUrl: './add-fund.page.html',
  styleUrls: ['./add-fund.page.scss'],
})
export class AddFundPage implements OnInit {

  isEditing = false;
  fundForm: FormGroup;
  showAutoComplete = false;
  searchResults: any[] = [];
  isSearched = false;
  transactionAmount: number;
  fundResultsData: any[] = [];

  constructor(
    private fb: FormBuilder,
    private store: Store,
  ) { }

  ngOnInit() {
    this.initForm();
    this.store.select(AppSelectors.searchMfResult).pipe(
      filter((result) => !!result && this.isSearched)
    ).subscribe((result) => {
      this.showAutoComplete = true;
      this.searchResults = result;
    });

    this.store.select(AppSelectors.fundDetails)
    .pipe(
      filter((result) => result?.meta && result?.data)
    )
    .subscribe((result) => {
      this.fundResultsData = result.data;
      this.fundForm.patchValue({
        fundType: result?.meta.scheme_type,
        fundCategory: result.meta.scheme_category,
        currentNav: result.data[0].nav,
        date: result.data[0].date.split('-').reverse().join('-')
      }, {
        emitEvent: false
      });
    });
  }

  private initForm() {
    this.fundForm = this.fb.group({
      fundCode: ['', [Validators.required]],
      fundName: ['', [Validators.required]],
      fundType: ['', [Validators.required]],
      fundCategory: ['', [Validators.required]],
      units: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      currentNav: ['', [Validators.required]],
      date: [this.setToday(), [Validators.required]]
    });
    this.fundForm.controls.fundName.valueChanges.pipe(
      debounceTime(500),
      filter((value) => value?.length > 3),
    ).subscribe((query) => {
      this.isSearched = true;
      this.isEditing = true;
      this.store.dispatch(new SearchMutualFunds(query));
    });
    this.fundForm.controls.amount.valueChanges.pipe(
      debounceTime(500),
      filter((value) => !!value && value > 0 && this.fundForm.value.currentNav),
      map((value) => parseFloat(value)),
    ).subscribe((amount) => {
      this.calculateAndUpdateUnits(amount);
    });
    this.fundForm.controls.date.valueChanges
    .pipe(
      filter((value) => value && this.fundForm.value.amount && this.fundResultsData?.length > 0)
    )
    .subscribe((date) => {
      let marketDate = parse(this.fundResultsData[0].date, 'dd-MM-yyyy', new Date());
      const enteredData = new Date(date);
      let nav;
      let amount;
      if (marketDate <= enteredData) {
        nav = this.fundResultsData[0].nav;
        amount = this.fundResultsData[0].amount;
      } else {
        let index = 1;
        while(!nav) {          
          marketDate = parse(this.fundResultsData[index].date, 'dd-MM-yyyy', new Date());
          if (marketDate <= enteredData) {
            nav = this.fundResultsData[index].nav;
          }
          index++;
        }
      }
      this.fundForm.patchValue({
        currentNav: nav,
        date: format(marketDate, 'yyyy-MM-dd')
      }, { emitEvent: false })
      this.calculateAndUpdateUnits(this.fundForm.value.amount);
    });
  }

  private calculateAndUpdateUnits(amount: number) {
    this.transactionAmount = amount - (amount * 0.00004);
      const nav = parseFloat(this.fundForm.value.currentNav);
      this.fundForm.patchValue({
        units: (this.transactionAmount / nav).toFixed(3)
      }, { emitEvent: false });
  }

  private setToday() {
    return new Date().toLocaleDateString().split('/').reverse().join('-');
  }

  selectResult(result) {
    this.fundForm.patchValue({
      fundCode: result.schemeCode,
      fundName: result.schemeName,
    }, {
      emitEvent: false
    });
    this.showAutoComplete = false;
    this.store.dispatch(new GetFundDetails(result.schemeCode));
  }

  addFund() {
    console.log(this.fundForm.value);
    if (this.fundForm.valid) {

    }
  }

}
