import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddFundPageRoutingModule } from './add-fund-routing.module';

import { AddFundPage } from './add-fund.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddFundPageRoutingModule,
    SharedModule,
    ReactiveFormsModule,   
  ],
  providers: [
  ],
  declarations: [AddFundPage]
})
export class AddFundPageModule {}
