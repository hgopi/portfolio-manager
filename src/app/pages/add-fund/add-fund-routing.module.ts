import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddFundPage } from './add-fund.page';

const routes: Routes = [
  {
    path: '',
    component: AddFundPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddFundPageRoutingModule {}
