import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { ChartConfiguration, ChartData, ChartOptions } from 'chart.js';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { GetCategories } from 'src/app/store/actions/app.actions';
import { AppSelectors } from 'src/app/store/selectors/app.selectors';
import { SqliteService } from '../../core/sqlite.service';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {

  hasInvestments: boolean;
  categories: any[] = [];
  selected = 1;
  pieChartOptions: ChartOptions<'pie'> = {
    responsive: false,
  };
  pieChartLabels = [];
  pieChartDatasets: any = [ {} ];
  pieChartLegend = true;
  pieChartPlugins = [];
  stock: any = {};
  mutual: any = {};
  gold: any = {};
  private destroy$ = new Subject<void>();

  constructor(
    private router: Router,
    private store: Store
  ) {
  }

  ngOnInit(): void {    
    this.store.select(AppSelectors.hasInvestments).subscribe((value) => this.hasInvestments = value);
    this.store.select(AppSelectors.categories).pipe(
      filter((categories) => categories?.length > 0),
      takeUntil(this.destroy$)
    ).subscribe((categories) => this.handleCategoriesData(categories));
  }

  ionViewWillEnter() {
    this.store.dispatch(new GetCategories());
  }

  handleCategoriesData(categories) {
    this.pieChartLabels = categories.map((category) => category.name);
    this.categories = categories;
    this.pieChartDatasets[0].data = categories.map((category) => {
      if (category.name === 'Stocks') this.stock = category;
      if (category.name === 'Mutual Funds') this.mutual = category;
      if (category.name === 'Gold') this.gold = category;
      return category.invested
    });
    console.log(this.categories[0]);
    this.selected = this.categories[0].id;
  }

  segmentChanged($event) {
    this.selected = parseInt($event.detail.value);
  }

  // navigateToPortfolio() {
  //   this.router.navigate(['/stock-portfolio']);
  // }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
// https://stackblitz.com/edit/ng2-charts-pie-template-v3?file=src%2Fapp%2Fapp.module.ts