import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StockPortfolioPageRoutingModule } from './stock-portfolio-routing.module';

import { StockPortfolioPage } from './stock-portfolio.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StockPortfolioPageRoutingModule,
    SharedModule,
  ],
  declarations: [StockPortfolioPage],
})
export class StockPortfolioPageModule {}
