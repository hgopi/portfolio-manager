import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StockPortfolioPage } from './stock-portfolio.page';

const routes: Routes = [
  {
    path: '',
    component: StockPortfolioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StockPortfolioPageRoutingModule {}
