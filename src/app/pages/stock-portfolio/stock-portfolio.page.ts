import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Actions, ofActionDispatched, Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AddStock, DeleteStock, GetStocks, StockDeleteSuccessfully } from 'src/app/store/actions/app.actions';
import { AppSelectors } from 'src/app/store/selectors/app.selectors';

@Component({
  selector: 'app-stock-portfolio',
  templateUrl: './stock-portfolio.page.html',
  styleUrls: ['./stock-portfolio.page.scss'],
})
export class StockPortfolioPage implements OnInit {

  @Select(AppSelectors.stocks) stocks$: Observable<any[]>;

  constructor(
    private store: Store,
    private router: Router,
    private toastController: ToastController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.store.dispatch(new GetStocks());
  }

  addStock() {
    this.router.navigate(['/add-stock']);
  }

  editStock(id: number) {
    alert(id);
    this.router.navigate([`/add-stock/${id}`]);
  }

  delete(slidingItem: any, id: number) {
    this.store.dispatch(new DeleteStock(id)).subscribe(async () => {
      const toast = await this.toastController.create({
        message: 'Stock Deleted Successfully',
        duration: 1000,
        icon: 'checkmark-done-outline'
      });      
      await toast.present();
      await toast.onDidDismiss();
      this.store.dispatch(new GetStocks());
    });
    setTimeout(() => slidingItem?.close(), 500);
  }

  doRefresh($event: any) {
    setTimeout(() => {
      console.log('Async operation has ended');
      $event.target.complete();
    }, 2000);
  }

}
