import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mf-portfolio',
  templateUrl: './mf-portfolio.page.html',
  styleUrls: ['./mf-portfolio.page.scss'],
})
export class MfPortfolioPage implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }
  
  addFunds() {
    this.router.navigate(['/add-fund']);
  }

}
