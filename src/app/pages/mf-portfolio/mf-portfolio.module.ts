import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MfPortfolioPageRoutingModule } from './mf-portfolio-routing.module';

import { MfPortfolioPage } from './mf-portfolio.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MfPortfolioPageRoutingModule,
    SharedModule,
  ],
  declarations: [MfPortfolioPage]
})
export class MfPortfolioPageModule {}
