import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MfPortfolioPage } from './mf-portfolio.page';

const routes: Routes = [
  {
    path: '',
    component: MfPortfolioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MfPortfolioPageRoutingModule {}
