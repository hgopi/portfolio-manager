import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import { filter } from 'rxjs/operators';
import { AddStock, EditStock, GetOneStockDetails, GetStockDetails, GetStocks, StockAddedSuccessfully } from 'src/app/store/actions/app.actions';
import { AppSelectors } from 'src/app/store/selectors/app.selectors';

@Component({
  selector: 'app-add-stock',
  templateUrl: './add-stock.page.html',
  styleUrls: ['./add-stock.page.scss'],
})
export class AddStockPage implements OnInit {

  stockForm: FormGroup;
  isEditing = false;
  stockId: number;

  constructor(
    private fb: FormBuilder,
    private store: Store,
    private toastController: ToastController,
    private actions$: Actions,
    private location: Location,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params
    .pipe(filter((params) => !!params?.id))
    .subscribe((params) => {
      this.isEditing = true;
      this.stockId = parseInt(params.id);
      this.store.dispatch(new GetOneStockDetails(this.stockId));
    });
    this.stockForm = this.fb.group({
      symbol: ['', [Validators.required]],
      name: ['', [Validators.required]],
      avgPrice: ['', [Validators.required]],
      quantity: ['', [Validators.required]],
      currMktPrice: ['', [Validators.required]],
      sector: ['', [Validators.required]],
      date: [this.getTodayDate(), [Validators.required]]
    });
    this.store.select(AppSelectors.getStockDetailsResult).subscribe((result) => {      
      console.log('getStockDetailsResult');
      this.stockForm.patchValue({
        currMktPrice: result?.priceInfo?.lastPrice,
        name: result?.info?.companyName,
        sector: result?.info?.industry
      });
    });
    this.actions$.pipe(
      ofActionDispatched(StockAddedSuccessfully)
    ).subscribe(async () => {
      const toast = await this.toastController.create({
        message: 'Stock Added Successfully',
        duration: 1000,
        icon: 'checkmark-done-outline'
      });
      await toast.present();
      await toast.onDidDismiss();
      this.location.back();
    });
    this.store.select(AppSelectors.editStockdata)
    .pipe(filter((response) => !!response))
    .subscribe((details) => {
      console.log('editStockData');
      this.stockForm.patchValue({
        symbol: details.symbol,
        name: details.name,
        avgPrice: details.avg_price,
        quantity: details.quantity,
        sector: details.sector
      });
      this.store.dispatch(new GetStockDetails(details.symbol));
    });
  }

  private getTodayDate() {
    const today = new Date();
    const month = `${today.getMonth() + 1}`.padStart(2, '0');
    const date = `${today.getDate()}`.padStart(2, '0');
    return `${date}/${month}/${today.getFullYear()}`
  }

  getStockDetails() {
    const { symbol } = this.stockForm.value;
    if (symbol) {
      this.store.dispatch(new GetStockDetails(symbol));
    }
  }

  async addStock() {
    console.log(this.stockForm.value);
    if (this.stockForm.valid) {
      if (this.isEditing) {
        this.store.dispatch(new EditStock(this.stockForm.value, this.stockId));
      } else {
        this.store.dispatch(new AddStock(this.stockForm.value));
      }
    } else {
      const toast = await this.toastController.create({
        message: 'Please fill all the required fields',
        duration: 2000,
        icon: 'alert-outline'
      });
      await toast.present();
    }
  }

}
