import { Injectable } from '@angular/core';
import { AppPreferences } from '@awesome-cordova-plugins/app-preferences/ngx';
import { from, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PreferenceService {

  constructor(
    private appPreferences: AppPreferences,
  ) { }

  _get(key: string) {
    return this.appPreferences.fetch(key);
  }

  get(key: string): Promise<any> {
    return this._get(key);
  }

  get$(key: string): Observable<any> {
    return from(this._get(key));
  }

  set(key: string, value: any) {
    return this.appPreferences.store(key, value);
  }

}
