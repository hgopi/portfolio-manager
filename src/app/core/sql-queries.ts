export class SqlQueries {
  static readonly dropCategoryTable = `DROP TABLE IF EXISTS category`;
  static readonly dropStocksTable = `DROP TABLE IF EXISTS stock`;
  static readonly createCategoryTable = `
    CREATE TABLE IF NOT EXISTS category(
      id INTEGER PRIMARY KEY,
      name  VARCHAR NOT NULL,
      invested NUMERIC DEFAULT 0,
      current_value NUMERIC DEFAULT 0,
      profit_loss NUMERIC DEFAULT 0,
      updated_at TEXT DEFAULT CURRENT_TIMESTAMP      
    )
  `;
  static readonly createStocksTable = `
  CREATE TABLE IF NOT EXISTS stock(
    id INTEGER PRIMARY KEY,
    name  VARCHAR NOT NULL,
    symbol VARCHAR NOT NULL,
    quantity NUMERIC,
    avg_price NUMERIC,
    total_cost NUMERIC,
    current_price NUMERIC,
    current_value NUMERIC,
    sector VARCHAR,
    updated_at TEXT DEFAULT CURRENT_TIMESTAMP
  ) 
  `;
  static readonly createMFTable = `
  CREATE TABLE IF NOT EXISTS mutual_funds(
    id INTEGER PRIMARY KEY,
    name  VARCHAR NOT NULL,
    code VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    category VARCHAR NOT NULL,
    avg_price NUMERIC,
    total_units NUMERIC,
    current_nav NUMERIC, 
    current_value NUMERIC,
    updated_at TEXT DEFAULT CURRENT_TIMESTAMP
  ) 
  `;
  static readonly createMFTransactionTable = `
  CREATE TABLE IF NOT EXISTS mutual_fund_transaction(
    id INTEGER PRIMARY KEY,
    fund_code INTEGER,
    total_units NUMERIC,
    total_nav NUMERIC, 
    updated_at TEXT DEFAULT CURRENT_TIMESTAMP
    FOREIGN KEY(fund_code) REFERENCES mutual_funds(code)
  ) 
  `;
  static readonly insertCategoryRows = `
    INSERT INTO category(id, name) VALUES (1, 'Stocks'), (2, 'Mutual Funds'), (3, 'Gold');
  `
  static readonly getCategories = `
    SELECT * FROM category
  `;
  static readonly addStock = `
    INSERT INTO stock 
      (id, name, symbol, quantity, avg_price, total_cost, current_price, current_value, sector, updated_at) 
      VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?, ?) 
  `;

  static readonly editStock = `
    UPDATE stock SET name = ?, symbol = ?, quantity = ?, avg_price = ?, total_cost = ?, current_price = ?, current_value = ?, sector = ?, updated_at = ? WHERE id = ?
  `;

  static readonly deletetStock = `
    DELETE FROM stock WHERE id = ?
  `;

  static readonly getStocks = `
    SELECT *, 
      printf("%.2f", current_value - total_cost) as pl,
      printf("%.2f", CAST((current_value - total_cost) * 100 AS REAL) / total_cost) as percent
    FROM stock
  `;

  static readonly getOneStockDetails = `
    SELECT * FROM stock where id = ? LIMIT 1
  `;

  static readonly getTotalStockInvestments = `
    SELECT SUM(total_cost) as total_investments, SUM(current_value) as net_worth, 
      (SUM(current_value) - SUM(total_cost)) as profit_loss FROM stock
  `;

  static readonly updateStockInvestments = `
    UPDATE category set invested = ?, current_value = ?, profit_loss = ?, updated_at = DATETIME('now') WHERE category.id = 1
  `
  static readonly createMfTransaction = `
    INSERT INTO mutual_fund_transaction (id, fund_code, total_units, total_nav, updated_at)
      VALUES (null, ?, ?, ?, DATETIME('now))
  `

  static readonly updateMfPorfolio = `
    INSERT INTO mutual_funds(id, name, code, type, category, avg_price, total_units, current_nav, current_value, updated_at)
      VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?, DATETIME('now))
  `

  
  // static readonly updateStockInvestments = `
  //   UPDATE category set invested = stocks.total_investments, current_value = stocks.net_worth, profit_loss = stocks.profit_loss, updated_at = DATETIME('now') FROM (
  //     SELECT SUM(total_cost) as total_investments, SUM(current_value) as net_worth, 
  //       (SUM(current_value) - SUM(total_cost)) as profit_loss FROM stock
  //   ) stocks
  //   WHERE category.id = 1
  // `;
}
