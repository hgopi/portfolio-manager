import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@awesome-cordova-plugins/sqlite/ngx';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { PreferenceService } from './preference.service';
import { SqlQueries } from './sql-queries';
import { map, switchMap } from 'rxjs/operators';
import { UpdatePortfolio } from '../shared/rxjs/update_portfolio';

@Injectable({
  providedIn: 'root'
})
export class SqliteService {

  private storage: SQLiteObject;
  private isDbInitiated = new BehaviorSubject<boolean>(false);
  public isDbInitiated$: Observable<boolean>;

  constructor(
    private sqlite: SQLite,
    private preferences: PreferenceService,
  ) {
    this.isDbInitiated$ = this.isDbInitiated.asObservable();
  }

  private mapRows(result: any) {
    let items: any[] = []; 
    for (let i = 0; i < result.rows.length; i++) {
      items.push(result.rows.item(i));
    }
    return items;
  }

  async init() {
    this.storage = await this.sqlite.create({
      name: 'data.db',
      location: 'default'
    });
    const hasInitiated = await this.preferences.get('dbinit');
    // const hasInitiated = false;
    if (!hasInitiated) {
      await this.preferences.set('dbinit', true);
      await this.storage.transaction((tx) => {
        // tx.executeSql(SqlQueries.dropCategoryTable);
        // tx.executeSql(SqlQueries.dropStocksTable);
        // tx.executeSql('DROP TABLE IF EXISTS mutual_funds');
        // tx.executeSql('DROP TABLE IF EXISTS mutual_fund_transaction');
        tx.executeSql(SqlQueries.createCategoryTable);
        tx.executeSql(SqlQueries.insertCategoryRows);        
        tx.executeSql(SqlQueries.createStocksTable);
        tx.executeSql(SqlQueries.createMFTable);
        tx.executeSql(SqlQueries.createMFTransactionTable);
      })      
    }
    this.isDbInitiated.next(true);
  }

  getCategories() {
    return from(this.storage.executeSql(SqlQueries.getCategories, [])).pipe(
      map(this.mapRows)
    );
  }

  addStock(stock: any) {
    const totalCost = stock.quantity * stock.avgPrice;
    const currentWorth = stock.quantity * stock.currMktPrice;
    return from(
      this.storage.transaction((tx) => {
        tx.executeSql(SqlQueries.addStock, [
          stock.name,
          stock.symbol,
          stock.quantity,
          stock.avgPrice,
          totalCost,
          stock.currMktPrice,
          currentWorth,
          stock.sector,
          stock.date
        ]);
      })).pipe(
        UpdatePortfolio(this.storage),
        // switchMap(() => {
        //   return from(this.storage.executeSql(SqlQueries.getTotalStockInvestments, [])).pipe(
        //     map(this.mapRows)
        //   )
        // }),
        // switchMap((details: any) => {
        //   return from(this.storage.executeSql(SqlQueries.updateStockInvestments, [
        //     details[0].total_investments,
        //     details[0].net_worth,
        //     details[0].profit_loss
        //   ]));
        // })
      );
  }

  editdStock(stock: any, id: number) {
    const totalCost = stock.quantity * stock.avgPrice;
    const currentWorth = stock.quantity * stock.currMktPrice;
    return from(
      this.storage.transaction((tx) => {
        this.storage.executeSql(SqlQueries.editStock, [
          stock.name,
          stock.symbol,
          stock.quantity,
          stock.avgPrice,
          totalCost,
          stock.currMktPrice,
          currentWorth,
          stock.sector,
          stock.date,
          id
        ]);        
      })
    ).pipe(
      UpdatePortfolio(this.storage),
      // switchMap(() => {
      //   return from(this.storage.executeSql(SqlQueries.getTotalStockInvestments, [])).pipe(
      //     map(this.mapRows)
      //   )
      // }),
      // switchMap((details: any) => {
      //   return from(this.storage.executeSql(SqlQueries.updateStockInvestments, [
      //     details[0].total_investments,
      //     details[0].net_worth,
      //     details[0].profit_loss
      //   ]));
      // })
    );
  }

  deleteStock(id: number) {
    return from(this.storage.executeSql(SqlQueries.deletetStock, [id]));
  }

  getStocks() {
    return from(this.storage.executeSql(SqlQueries.getStocks, [])).pipe(
      map(this.mapRows)
    );
  }

  getOneStockDetails(id) {
    return from(this.storage.executeSql(SqlQueries.getOneStockDetails, [id])).pipe(
      map(this.mapRows)
    );
  }



}
