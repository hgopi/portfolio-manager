import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HTTP } from '@awesome-cordova-plugins/http/ngx';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class HttpService {

  constructor(
    private http: HTTP,
    private httpClient: HttpClient
  ) { }

  getStockDetails(symbol: string) {
    const url = `https://www.nseindia.com/api/quote-equity?symbol=${symbol}`;
    return from(this.http.get(url, {}, {
      // 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36',
    })).pipe(
      map((response) => {
        if (response?.data) {
          return JSON.parse(response.data);
        } else {
          return null;
        }
      })
    );
  }

  searchFunds(query: string) {
    const url = `https://api.mfapi.in/mf/search?q=${encodeURI(query)}`;
    return from(this.http.get(url, {}, {})).pipe(
      map((response) => {
        if (response?.data) {
          return JSON.parse(response.data);
        } else {
          return null;
        }
      })
    )
  }

  getFundDetails(schemeCode: string | number) {
    const url = `https://api.mfapi.in/mf/${schemeCode}`;
    return from(this.http.get(url, {}, {})).pipe(
      map((response) => {
        if (response?.data) {
          return JSON.parse(response.data);
        } else {
          return null;
        }
      })
    )
  }

}
