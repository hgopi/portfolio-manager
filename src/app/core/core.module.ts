import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreferenceService } from './preference.service';
import { AppPreferences } from '@awesome-cordova-plugins/app-preferences/ngx';
import { SQLite } from '@awesome-cordova-plugins/sqlite/ngx';
import { HttpClientModule } from '@angular/common/http';
import { HTTP } from '@awesome-cordova-plugins/http/ngx';
import { HttpService } from './http.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,    
    HttpClientModule,
  ],
  providers: [
    HTTP,
    PreferenceService,
    AppPreferences,
    HttpService,
    SQLite,
  ]
})
export class CoreModule { }
