import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-net-worth',
  templateUrl: './net-worth.component.html',
  styleUrls: ['./net-worth.component.scss'],
})
export class NetWorthComponent implements OnInit {

  @Input() type = '';
  @Input() details: any = {};

  constructor(
    private router: Router
  ) { }

  ngOnInit() {}

  navigateToPortfolio() {
    if (this.type === 'stock') {
      this.router.navigate(['/stock-portfolio']);
    } else if (this.type === 'mf') {
      this.router.navigate(['/mf-portfolio']);
    }
  }

}
