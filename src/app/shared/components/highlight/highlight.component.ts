import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-highlight',
  templateUrl: './highlight.component.html',
  styleUrls: ['./highlight.component.scss'],
})
export class HighlightComponent implements OnInit {
  @Input() amount: number = 0;
  @Input() cssClass: string = '';
  @Input() percentage = false;

  constructor() {}

  ngOnInit() {
    this.cssClass += this.amount < 0 ? ' text-red-500' : ' text-green-500';
    this.cssClass += ' text-right';
  }
}
