import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightComponent } from './components/highlight/highlight.component';
import { NetWorthComponent } from './components/net-worth/net-worth.component';



@NgModule({
  declarations: [
    HighlightComponent,
    NetWorthComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    HighlightComponent,
    NetWorthComponent,
  ]
})
export class SharedModule { }
