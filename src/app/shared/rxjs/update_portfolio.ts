import { SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { from, Observable } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { SqlQueries } from "src/app/core/sql-queries";

export function UpdatePortfolio(storage: SQLiteObject) {
  return function<T>(source: Observable<T>): Observable<T> {
    return new Observable((subscriber) => {
      const subscription = source.pipe(
        switchMap(() => {
          return from(storage.executeSql(SqlQueries.getTotalStockInvestments, [])).pipe(
            map((result) => {
              let items: any[] = []; 
              for (let i = 0; i < result.rows.length; i++) {
                items.push(result.rows.item(i));
              }
              return items;
            })
          )
        }),
        switchMap((details: any) => {
          return from(storage.executeSql(SqlQueries.updateStockInvestments, [
            details[0].total_investments,
            details[0].net_worth,
            details[0].profit_loss
          ]));
        })
      ).subscribe({
        next(value) {
          subscriber.next(value);
        },
        error(error) {
          subscriber.error(error);
        },
        complete() {
          subscriber.complete();
        }
      });
      return () => subscription.unsubscribe();
    });
  }
}