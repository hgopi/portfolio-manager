import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import { filter } from 'rxjs/operators';
import { SqliteService } from './core/sqlite.service';
import { HideLoader, ShowLoader } from './store/actions/app.actions';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {

  loader: HTMLIonLoadingElement;
  isLoading = false;

  constructor(
    private sqliteService: SqliteService,
    private loadingController: LoadingController,
    private actions$: Actions
  ) {}

  ngOnInit(): void {
    this.init();
    this.actions$.pipe(
      ofActionDispatched(ShowLoader),
      filter(() => this.loader && !this.isLoading)
    ).subscribe(() => {
      this.isLoading = true;
      this.loader.present();
    });
    this.actions$.pipe(
      ofActionDispatched(HideLoader),
    ).subscribe(() => {
      this.isLoading = false;
      this.loader?.dismiss();
    });
  }

  async init() {
    try {
      await this.sqliteService.init();
    } catch(e) {
      alert(e.message);
    }
    this.loader = await this.loadingController.create({
      message: 'Loading...',
      translucent: true,
      backdropDismiss: false
    });
  }

}
