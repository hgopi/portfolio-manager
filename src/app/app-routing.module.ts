import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },  
  {
    path: 'stock-portfolio',
    loadChildren: () => import('./pages/stock-portfolio/stock-portfolio.module').then( m => m.StockPortfolioPageModule)
  },
  {
    path: 'add-stock',
    loadChildren: () => import('./pages/add-stock/add-stock.module').then( m => m.AddStockPageModule)
  },
  {
    path: 'mf-portfolio',
    loadChildren: () => import('./pages/mf-portfolio/mf-portfolio.module').then( m => m.MfPortfolioPageModule)
  },  
  {
    path: 'add-fund',
    loadChildren: () => import('./pages/add-fund/add-fund.module').then( m => m.AddFundPageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
