import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.vigowebs.pm',
  appName: 'Portfolio Manager',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
